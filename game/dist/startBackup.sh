#!/bin/bash
date=`date '+%m-%d-%y'`
###############################################
## Configurate Database Connections please!!!!
###############################################
## Please, type here you dir to mysql directory bin. Example: /home/my_user/MySQL5/bin
mysqlPath="/db"
## Please, type here you dir to save backup directory. Example: /home/my_user/backup
backupPath="/db/backup"
user="root"
pass="root"
db="l2jdb"

echo "Start backuping"
$mysqlPath/mysqldump -u $user -p $pass $db>$backupPath/$db.$date.sql
gzip $backupPath/$db.$date.sql
rm $backupPath/$db.$date.sql