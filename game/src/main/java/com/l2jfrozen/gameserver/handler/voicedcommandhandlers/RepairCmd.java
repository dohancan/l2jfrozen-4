package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterItemEntity;
import com.l2jfrozen.database.model.game.ItemLocation;
import com.l2jfrozen.gameserver.cache.HtmCache;
import com.l2jfrozen.gameserver.handler.ICustomByPassHandler;
import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Command .repair
 *
 * @author L2JFrozen
 */
public class RepairCmd implements IVoicedCommandHandler, ICustomByPassHandler {
    static final Logger LOGGER = LoggerFactory.getLogger(RepairCmd.class.getName());

    private static final String[] _voicedCommands =
            {
                    "repair",
            };

    @Override
    public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target) {
        if (activeChar == null) {
            return false;
        }

        // Send activeChar HTML page
        if (command.startsWith("repair")) {
            String htmContent = HtmCache.getInstance().getHtm("data/html/mods/repair/repair.htm");
            NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(5);
            npcHtmlMessage.setHtml(htmContent);
            npcHtmlMessage.replace("%acc_chars%", getCharList(activeChar));
            activeChar.sendPacket(npcHtmlMessage);
            return true;
        }
        return false;
    }

    private String getCharList(L2PcInstance activeChar) {
        String result = "";
        String repCharAcc = activeChar.getAccountName();


        List<CharacterEntity> characters = CharacterManager.getInstance().get(repCharAcc);
        if (characters.isEmpty()) {
            return result;
        }
        for (CharacterEntity character : characters) {
            if (activeChar.getName().compareTo(character.getCharName()) != 0) {
                result += activeChar.getName() + ";";
            }
        }

        return result;
    }

    private boolean checkAcc(L2PcInstance activeChar, String repairChar) {
        boolean result = false;
        String repCharAcc = "";

        CharacterEntity character = CharacterManager.getInstance().getByCharacterName(repairChar);
        if (character == null) {
            return false;
        }
        repCharAcc = character.getAccountName();

        if (activeChar.getAccountName().compareTo(repCharAcc) == 0) {
            result = true;
        }
        return result;
    }

    private boolean checkPunish(L2PcInstance activeChar, String repairChar) {
        boolean result = false;
        int accessLevel = 0;
        int repCharJail = 0;


        CharacterEntity character = CharacterManager.getInstance().getByCharacterName(repairChar);
        if (character == null) {
            return false;
        }
        accessLevel = character.getAccessLevel().ordinal();
        repCharJail = character.getPunishLevel();
        if (repCharJail == 1 || accessLevel < 0) // 0 norm, 1 chat ban, 2 jail, 3....
        {
            result = true;
        }
        return result;
    }

    private boolean checkKarma(L2PcInstance activeChar, String repairChar) {
        boolean result = false;
        int repCharKarma = 0;

        CharacterEntity character = CharacterManager.getInstance().getByCharacterName(repairChar);
        if (character == null) {
            return false;
        }
        repCharKarma = character.getKarma();

        if (repCharKarma > 0) {
            result = true;
        }
        return result;
    }

    private boolean checkChar(L2PcInstance activeChar, String repairChar) {
        boolean result = false;
        if (activeChar.getName().compareTo(repairChar) == 0) {
            result = true;
        }
        return result;
    }

    private void repairBadCharacter(String charName) {
        try {
            L2PcInstance player=L2World.getInstance().getPlayer(charName);
            if(player==null){
                return;
            }
            CharacterEntity character = player.getCharacter();
            if (character == null) {
                return;
            }
            long objId = character.getId();
            if (objId == 0) {
                return;
            }
            character.setX(17867);
            character.setY(170259);
            character.setZ(-3503);
            CharacterManager.getInstance().deleteShortcuts(character);
            for(CharacterItemEntity item:character.getItems().values()){
                if(item.getLoc()==ItemLocation.PAPERDOLL){
                    item.setLoc(ItemLocation.INVENTORY);
                }
            }
        } catch (Exception e) {
            LOGGER.warn("GameServer: could not repair character:",e);
        }
    }

    @Override
    public String[] getVoicedCommandList() {
        return _voicedCommands;
    }


    private static final String[] _BYPASSCMD = {"repair", "repair_close_win"};

    private enum CommandEnum {
        repair,
        repair_close_win
    }

    @Override
    public String[] getByPassCommands() {
        return _BYPASSCMD;
    }

    /* (non-Javadoc)
     * @see com.l2jfrozen.gameserver.handler.ICustomByPassHandler#handleCommand(java.lang.String, com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance, java.lang.String)
     */
    @Override
    public void handleCommand(String command, L2PcInstance activeChar, String repairChar) {

        CommandEnum comm = CommandEnum.valueOf(command);

        if (comm == null) {
            return;
        }

        switch (comm) {
            case repair: {

                if (repairChar == null
                        || repairChar.equals("")) {
                    return;
                }

                if (checkAcc(activeChar, repairChar)) {
                    if (checkChar(activeChar, repairChar)) {
                        String htmContent = HtmCache.getInstance().getHtm("data/html/mods/repair/repair-self.htm");
                        NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(5);
                        npcHtmlMessage.setHtml(htmContent);
                        activeChar.sendPacket(npcHtmlMessage);
                        return;
                    } else if (checkPunish(activeChar, repairChar)) {
                        String htmContent = HtmCache.getInstance().getHtm("data/html/mods/repair/repair-jail.htm");
                        NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(5);
                        npcHtmlMessage.setHtml(htmContent);
                        activeChar.sendPacket(npcHtmlMessage);
                        return;
                    } else if (checkKarma(activeChar, repairChar)) {
                        activeChar.sendMessage("Selected Char has Karma,Cannot be repaired!");
                        return;
                    } else {
                        repairBadCharacter(repairChar);
                        String htmContent = HtmCache.getInstance().getHtm("data/html/mods/repair/repair-done.htm");
                        NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(5);
                        npcHtmlMessage.setHtml(htmContent);
                        activeChar.sendPacket(npcHtmlMessage);
                        return;
                    }
                }

                String htmContent = HtmCache.getInstance().getHtm("data/html/mods/repair/repair-error.htm");
                NpcHtmlMessage npcHtmlMessage = new NpcHtmlMessage(5);
                npcHtmlMessage.setHtml(htmContent);
                npcHtmlMessage.replace("%acc_chars%", getCharList(activeChar));
                activeChar.sendPacket(npcHtmlMessage);
                return;
            }
            case repair_close_win: {
                return;
            }
        }

    }
}

