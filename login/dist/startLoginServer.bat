@echo off
title L2J-Frozen: Login Server Console
:start
echo ------------------------------
echo Starting LoginServer...
echo Website : www.l2jfrozen.com
echo ------------------------------
echo.

java -jar -Dcom.sun.management.jmxremote.port=10998 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dfile.encoding=UTF8 -Xms128m -Xmx128m login-1.0-SNAPSHOT.jar

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Admin Restarted ...
ping -n 5 localhost > nul
echo.
goto start
:error
echo.
echo LoginServer terminated abnormaly
ping -n 5 localhost > nul
echo.
goto start
:end
echo.
echo LoginServer terminated
echo.
:question
set choix=q
set /p choix=Restart(r) or Quit(q)
if /i %choix%==r goto start
if /i %choix%==q goto exit
:exit
exit
pause
