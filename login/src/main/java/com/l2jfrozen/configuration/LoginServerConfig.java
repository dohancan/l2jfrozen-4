package com.l2jfrozen.configuration;

import com.l2jfrozen.context.SpringApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.ServerTitle;
import com.l2jfrozen.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Properties;

/**
 * User: vdidenko Date: 11/21/13 Time: 10:58 PM
 */
@Service
public class LoginServerConfig {
	public static boolean ENABLE_ALL_EXCEPTIONS = true;
	public String GAME_SERVER_LOGIN_HOST;
	public String INTERNAL_HOSTNAME;
	public String EXTERNAL_HOSTNAME;
	public int GAME_SERVER_LOGIN_PORT;
	public String LOGIN_BIND_ADDRESS;
	public int LOGIN_TRY_BEFORE_BAN;
	public int LOGIN_BLOCK_AFTER_BAN;
	public int PORT_LOGIN;
	public static boolean ACCEPT_NEW_GAMESERVER;
	public int DATABASE_TIMEOUT;
	public int DATABASE_CONNECTION_TIMEOUT;
	public int DATABASE_PARTITION_COUNT;
	public static boolean ENABLE_DDOS_PROTECTION_SYSTEM;
	public static boolean ENABLE_DEBUG_DDOS_PROTECTION_SYSTEM;
	public static String DDOS_COMMAND_BLOCK;
	public static int BRUT_AVG_TIME;
	public static int BRUT_LOGON_ATTEMPTS;
	public static int BRUT_BAN_IP_TIME;
	public static boolean SHOW_LICENCE;
	public boolean FORCE_GGAUTH;
	public boolean FLOOD_PROTECTION;
	public int FAST_CONNECTION_LIMIT;
	public int NORMAL_CONNECTION_TIME;
	public int FAST_CONNECTION_TIME;
	public int MAX_CONNECTION_PER_IP;
	public static boolean AUTO_CREATE_ACCOUNTS;
	public static String NETWORK_IP_LIST;
	public static long SESSION_TTL;
	public static int MAX_LOGINSESSIONS;
	public int IP_UPDATE_TIME;
	public static boolean DEBUG;
	public boolean DEVELOPER;
	public static boolean DEBUG_PACKETS;
	public boolean IS_TELNET_ENABLED;
    @Autowired
    private ConfigManager configManager;
    
	private static final Logger LOGGER = LoggerFactory
			.getLogger(LoginServerConfig.class);

	public LoginServerConfig() {

		Util.printSection("Team");

		// Print L2jfrozen's Logo
		ServerTitle.info();

		Util.printSection("Configs");
		LOGGER.info("Initialize login config");
	}

	@PostConstruct
	public void load() {
		GAME_SERVER_LOGIN_HOST = configManager.getString("LoginHostname");
		GAME_SERVER_LOGIN_PORT =configManager.getInteger("LoginPort");

		LOGIN_BIND_ADDRESS = configManager.getString("LoginserverHostname");
		PORT_LOGIN = configManager.getInteger("LoginserverPort");

		ACCEPT_NEW_GAMESERVER =configManager.getBoolean("AcceptNewGameServer");

		LOGIN_TRY_BEFORE_BAN = configManager.getInteger("LoginTryBeforeBan");
		LOGIN_BLOCK_AFTER_BAN = configManager.getInteger("LoginBlockAfterBan");

		INTERNAL_HOSTNAME = configManager.getString("InternalHostname");
		EXTERNAL_HOSTNAME = configManager.getString("ExternalHostname");

		ENABLE_DDOS_PROTECTION_SYSTEM = configManager.getBoolean("EnableDdosProSystem");
		DDOS_COMMAND_BLOCK = configManager.getString("Deny_noallow_ip_ddos");
		ENABLE_DEBUG_DDOS_PROTECTION_SYSTEM = configManager.getBoolean("Fulllog_mode_print");

		DATABASE_TIMEOUT = configManager.getInteger("LoginTimeOutConDb");
		DATABASE_CONNECTION_TIMEOUT = configManager.getInteger("SingleConnectionTimeOutDb");
		DATABASE_PARTITION_COUNT = configManager.getInteger("LoginPartitionCount");

		// Anti Brute force attack on login
		BRUT_AVG_TIME = configManager.getInteger("BrutAvgTime"); // in Seconds
		BRUT_LOGON_ATTEMPTS = configManager.getInteger("BrutLogonAttempts");
		BRUT_BAN_IP_TIME = configManager.getInteger("BrutBanIpTime"); // in
																		// Seconds

		SHOW_LICENCE = configManager.getBoolean("ShowLicence");
		IP_UPDATE_TIME = configManager.getInteger("IpUpdateTime");
		FORCE_GGAUTH = configManager.getBoolean("ForceGGAuth");

		AUTO_CREATE_ACCOUNTS = configManager.getBoolean("AutoCreateAccounts");

		FLOOD_PROTECTION = configManager.getBoolean("EnableFloodProtection");
		FAST_CONNECTION_LIMIT = configManager.getInteger("FastConnectionLimit");
		NORMAL_CONNECTION_TIME = configManager.getInteger("NormalConnectionTime");
		FAST_CONNECTION_TIME = configManager.getInteger("FastConnectionTime");
		MAX_CONNECTION_PER_IP = configManager.getInteger("MaxConnectionPerIP");
		DEBUG = configManager.getBoolean("LoginDebug");
		DEVELOPER = configManager.getBoolean("LoginDeveloper");

		NETWORK_IP_LIST = configManager.getString("NetworkList");
		SESSION_TTL =Long.decode(configManager.getString("SessionTTL"));
		MAX_LOGINSESSIONS = configManager.getInteger("MaxSessions");

		DEBUG_PACKETS = configManager.getBoolean("LoginDebugPackets");
		IS_TELNET_ENABLED = configManager.getBoolean("EnableTelnet");
		ENABLE_ALL_EXCEPTIONS = configManager.getBoolean("EnableAllExceptionsLog");
	}
}