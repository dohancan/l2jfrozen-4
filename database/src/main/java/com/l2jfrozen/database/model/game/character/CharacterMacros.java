package com.l2jfrozen.database.model.game.character;

import com.l2jfrozen.database.model.AbstractIdentifiable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * author vadim.didenko
 * 1/12/14.
 */
@Entity
@Table(name = "character_macros")
public class CharacterMacros extends AbstractIdentifiable {

    private CharacterEntity character;
    //private int macrosId;
    private Integer icon;

    private String name;

    private String descr;

    private String acronym;

    private String commands;

//    @Column(name = "macros_id")
//    public int getMacrosId() {
//        return macrosId;
//    }
//
//    public void setMacrosId(int macrosId) {
//        this.macrosId = macrosId;
//    }

    @Column(name = "acronym")
    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "char_obj_id")
    public CharacterEntity getCharacter() {
        return character;
    }

    public void setCharacter(CharacterEntity charObjId) {
        this.character = charObjId;
    }

    @Basic
    @Column(name = "commands")
    public String getCommands() {
        return commands;
    }

    public void setCommands(String commands) {
        this.commands = commands;
    }

    @Basic
    @Column(name = "descr")
    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Basic
    @Column(name = "icon")
    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
