package com.l2jfrozen.database;

import org.hibernate.engine.spi.SessionImplementor;

import java.io.Serializable;

/**
 * author vadim.didenko
 * 1/13/14.
 */
public class IdGenerator extends org.hibernate.id.IdentityGenerator {
    @Override
    public Serializable generate(SessionImplementor s, Object obj) {
        return System.nanoTime();
    }
}
