package com.l2jfrozen.database.exception;

/**
 * Generic manager exception.
 */
public class ManagerException extends Exception {
    /**
     * Default constructor.
     *
     * @param message message
     */
    public ManagerException(String message) {
        super(message);
    }

    /**
     * Constructor with cause.
     *
     * @param message message
     * @param cause   cause
     */
    public ManagerException(String message, Throwable cause) {
        super(message, cause);
    }
}
