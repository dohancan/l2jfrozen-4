package com.l2jfrozen.database.dal.login;

import com.l2jfrozen.database.model.login.GameServer;

import java.util.List;

/**
 * author vadim.didenko
 * 1/11/14.
 */
public interface GameServerManagerDAO {
    GameServer add(GameServer gameServer);

    void clear();

    void delete(GameServer gameServer);

    GameServer get(Integer id);
    List<GameServer> getAll();
}
